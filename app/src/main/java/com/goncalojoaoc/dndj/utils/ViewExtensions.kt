package com.goncalojoaoc.dndj.utils

import android.content.Context
import android.content.ContextWrapper
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.STATE_COLLAPSED
import com.google.android.material.bottomsheet.BottomSheetBehavior.STATE_EXPANDED

fun View.hide() {
    visibility = GONE
}

fun View.show() {
    visibility = VISIBLE
}

fun View.show(shouldShow: Boolean) {
    visibility = if (shouldShow) VISIBLE else GONE
}

fun <T : View> BottomSheetBehavior<T>.collapse() {
    state = STATE_COLLAPSED
}

fun <T : View> BottomSheetBehavior<T>.expand() {
    state = STATE_EXPANDED
}

fun Fragment.notImplemented(@StringRes str: Int) {
    showToast(str)
}

fun Fragment.showToast(@StringRes str: Int, length: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(requireContext(), requireContext().getText(str), length).show()
}

fun Context.getActivity(): AppCompatActivity? = if (this is ContextWrapper) {
    if (this is AppCompatActivity) {
        this
    } else {
        this.baseContext.getActivity()
    }
} else {
    null
}
