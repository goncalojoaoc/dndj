package com.goncalojoaoc.dndj.utils

import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

val <T, VH : RecyclerView.ViewHolder> ListAdapter<T, VH>.isEmpty
    get() = itemCount == 0

val <T, VH : RecyclerView.ViewHolder> ListAdapter<T, VH>.isNotEmpty
    get() = itemCount != 0
