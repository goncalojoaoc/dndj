package com.goncalojoaoc.dndj.strings

import android.content.Context
import androidx.annotation.StringRes
import dagger.hilt.android.qualifiers.ActivityContext
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class StringResources @Inject constructor(
    @ApplicationContext private val context: Context
) : IStrings {
    override fun getById(@StringRes id: Int): String = context.getString(id)

    override fun getById(id: Int, vararg formatArgs: Any) = context.getString(id, *formatArgs)

}