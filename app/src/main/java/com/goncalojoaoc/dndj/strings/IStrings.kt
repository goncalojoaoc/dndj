package com.goncalojoaoc.dndj.strings

import androidx.annotation.StringRes

interface IStrings {

    fun getById(@StringRes id: Int): String

    fun getById(@StringRes id: Int, vararg formatArgs: Any): String
}
