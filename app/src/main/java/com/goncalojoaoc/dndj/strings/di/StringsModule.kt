package com.goncalojoaoc.dndj.strings.di

import com.goncalojoaoc.dndj.strings.IStrings
import com.goncalojoaoc.dndj.strings.StringResources
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class StringsModule {

    @Binds
    abstract fun bindStrings(impl: StringResources): IStrings
}
