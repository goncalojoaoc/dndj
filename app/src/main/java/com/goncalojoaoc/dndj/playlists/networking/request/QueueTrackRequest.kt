package com.goncalojoaoc.dndj.playlists.networking.request

data class QueueTrackRequest(
    val query: String,
    val startImmediately: Boolean
)
