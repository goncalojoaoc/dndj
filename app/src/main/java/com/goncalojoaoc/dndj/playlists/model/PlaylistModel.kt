package com.goncalojoaoc.dndj.playlists.model

data class PlaylistModel(
    val id: Long,
    val url: String,
    val name: String,
    val imageUri: String,
    val source: PlaylistSourceModel
)
