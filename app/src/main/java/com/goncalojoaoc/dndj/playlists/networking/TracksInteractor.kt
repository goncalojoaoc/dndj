package com.goncalojoaoc.dndj.playlists.networking

import com.goncalojoaoc.dndj.networking.BotBackendClientManager
import com.goncalojoaoc.dndj.networking.safeApiCall
import com.goncalojoaoc.dndj.playlists.model.PlaylistModel
import com.goncalojoaoc.dndj.playlists.networking.request.QueueTrackRequest
import javax.inject.Inject

class TracksInteractor @Inject constructor(
    private val clientManager: BotBackendClientManager
) {
    suspend fun play(playlistModel: PlaylistModel) = safeApiCall {
        clientManager.tracksClient.queue(
            QueueTrackRequest(
                query = playlistModel.url,
                startImmediately = true
            )
        )
    }
}
