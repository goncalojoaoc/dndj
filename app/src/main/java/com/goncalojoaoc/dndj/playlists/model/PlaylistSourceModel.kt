package com.goncalojoaoc.dndj.playlists.model

import androidx.annotation.StringRes
import com.goncalojoaoc.dndj.R

enum class PlaylistSourceModel(
    @StringRes val displayName: Int,
    @StringRes val label: Int
) {
    YOUTUBE(R.string.playlist_source_youtube, R.string.playlist_youtube_url)
}