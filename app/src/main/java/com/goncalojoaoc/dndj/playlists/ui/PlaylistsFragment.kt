package com.goncalojoaoc.dndj.playlists.ui

import android.os.Bundle
import android.view.*
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView.VERTICAL
import com.goncalojoaoc.dndj.ErrorTrackingViewModel
import com.goncalojoaoc.dndj.R
import com.goncalojoaoc.dndj.channels.ui.ChannelsBottomSheet
import com.goncalojoaoc.dndj.channels.ui.ChannelsViewModel
import com.goncalojoaoc.dndj.databinding.ErrorBannerViewBinding
import com.goncalojoaoc.dndj.databinding.PlaylistsFragmentBinding
import com.goncalojoaoc.dndj.error.ui.BaseErrorBannerFragment
import com.goncalojoaoc.dndj.error.ui.ErrorBanner
import com.goncalojoaoc.dndj.folders.model.FolderModel
import com.goncalojoaoc.dndj.strings.IStrings
import com.goncalojoaoc.dndj.viewutils.GridSpacingItemDecoration
import com.goncalojoaoc.dndj.viewutils.SwipeableRecyclerView.OnSwipeListener
import com.google.android.material.tabs.TabLayout
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class PlaylistsFragment : BaseErrorBannerFragment(), OnSwipeListener {

    private val playlistsViewModel: PlaylistsViewModel by viewModels()
    private val channelsViewModel: ChannelsViewModel by viewModels()

    @Inject
    lateinit var strings: IStrings
    private lateinit var binding: PlaylistsFragmentBinding

    override val errorBannerViewBinding: ErrorBannerViewBinding
        get() = binding.errorBanner

    override val errorTrackingViewModel: ErrorTrackingViewModel
        get() = channelsViewModel

    private lateinit var adapter: PlaylistAdapter
    private lateinit var errorBanner: ErrorBanner

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = PlaylistsFragmentBinding.inflate(inflater, container, false)
        errorBanner = ErrorBanner(binding.errorBanner)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupBottomSheet()
        setupTabs()
        setupRecyclerView()
        setupCurrentlyPlaying()
    }

    private fun setupCurrentlyPlaying() {
        binding.currentlyPlaying.setShouldPollForInfo {
            channelsViewModel.isInVoiceChannel()
        }
    }

    private fun setupTabs() {
        binding.tabLayout.addOnTabSelectedListener(OnFolderSelectedListener())
        playlistsViewModel.folders.observe(viewLifecycleOwner, { folders ->
            binding.updateTabs(folders)
            binding.tabLayout.addOnLongClickListener()
        })
    }

    private fun PlaylistsFragmentBinding.updateTabs(folders: List<FolderModel>) {
        tabLayout.removeAllTabs()
        folders.forEach { folder ->
            tabLayout.addTab(tabLayout.newTab().setText(folder.name).setTag(folder))
        }
    }

    private fun selectedFolder(): FolderModel? = with(binding.tabLayout) {
        val index = selectedTabPosition
        getTabAt(index)?.tag as? FolderModel
    }

    private fun setupBottomSheet() {
        ChannelsBottomSheet(
            binding = binding.channelsBottomSheet,
            lifecycleOwner = this,
            viewModel = channelsViewModel,
            strings = strings
        )
    }

    private fun setupRecyclerView() {
        adapter = PlaylistAdapter(playlistsViewModel::play, playlistsViewModel::delete)
        with(binding) {
            recyclerView.layoutManager =
                GridLayoutManager(requireContext(), 2, VERTICAL, false)
            recyclerView.addItemDecoration(
                GridSpacingItemDecoration(
                    2,
                    resources.getDimensionPixelOffset(R.dimen.playlist_column_spacing),
                    false
                )
            )
            recyclerView.adapter = adapter
        }
        binding.recyclerView.swipeListener = this
        playlistsViewModel.playlists.observe(viewLifecycleOwner, {
            adapter.clearEditingState()
            adapter.submitList(it)
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.main_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            R.id.add_playlist -> goToCreatePlaylist()
            R.id.server_connection -> goToServerSetup()
            R.id.add_folder -> goToCreateFolder()
            else -> false
        }

    private fun goToServerSetup(): Boolean {
        findNavController().navigate(
            if (errorBanner.isShowing) {
                PlaylistsFragmentDirections.actionMainFragmentToAuthenticationFragment()
            } else {
                PlaylistsFragmentDirections.actionMainFragmentToServerConnectionsFragment()
            }
        )
        return true
    }

    private fun goToCreateFolder(): Boolean {
        findNavController().navigate(
            PlaylistsFragmentDirections.actionMainFragmentToFolderDetailsFragment()
        )
        return true
    }

    private fun goToEditFolder(folder: FolderModel): Boolean {
        findNavController().navigate(
            PlaylistsFragmentDirections.actionMainFragmentToFolderDetailsFragment(folder)
        )
        return true
    }

    private fun goToCreatePlaylist(): Boolean {
        findNavController().navigate(
            PlaylistsFragmentDirections.actionMainFragmentToPlaylistDetailsFragment(
                selectedFolder()?.id ?: -1
            )
        )
        return true
    }

    private fun TabLayout.next() {
        if (selectedTabPosition < tabCount) {
            val toSelect = getTabAt(selectedTabPosition + 1)
            selectTab(toSelect)
        }
    }

    private fun TabLayout.previous() {
        if (selectedTabPosition > 0) {
            val toSelect = getTabAt(selectedTabPosition - 1)
            selectTab(toSelect)
        }
    }

    private fun TabLayout.addOnLongClickListener() {

        class TabEditClickListener(
            private val tab: TabLayout.Tab
        ) : View.OnLongClickListener {
            override fun onLongClick(v: View?): Boolean {
                return (tab.tag as? FolderModel)?.let {
                    goToEditFolder(it)
                } ?: false
            }
        }

        for (i in 0..childCount) {
            val tab = getTabAt(i)
            tab?.view?.setOnLongClickListener(TabEditClickListener(tab))
        }
    }

    override fun onSwipeLeft() {
        binding.tabLayout.next()
    }

    override fun onSwipeRight() {
        binding.tabLayout.previous()
    }

    private inner class OnFolderSelectedListener : TabLayout.OnTabSelectedListener {

        override fun onTabSelected(tab: TabLayout.Tab?) {
            (tab?.tag as? FolderModel)?.let { selectedFolder ->
                playlistsViewModel.loadFromFolder(selectedFolder)
            }
        }

        override fun onTabUnselected(tab: TabLayout.Tab?) {
            // no-op
        }

        override fun onTabReselected(tab: TabLayout.Tab?) {
            // no-op
        }
    }

}
