package com.goncalojoaoc.dndj.playlists.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Playlist(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,
    val folderId: Long = 0, // TODO: ignore for now
    val url: String,
    val imageUri: String,
    val name: String,
    val source: PlaylistSource
)