package com.goncalojoaoc.dndj.playlists.repository

import androidx.lifecycle.LiveData
import com.goncalojoaoc.dndj.playlists.model.PlaylistModel

interface IPlaylistRepository {

    fun getAll(): LiveData<List<PlaylistModel>>

    fun getAllInFolder(folderId: Long): LiveData<List<PlaylistModel>>

    fun getById(): LiveData<PlaylistModel>

    suspend fun create(model: PlaylistModel, folderId: Long): Long

    suspend fun delete(model: PlaylistModel)
}
