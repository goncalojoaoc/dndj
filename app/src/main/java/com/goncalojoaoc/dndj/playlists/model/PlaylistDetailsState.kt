package com.goncalojoaoc.dndj.playlists.model

data class PlaylistDetailsState(
    val titleError: String? = null,
    val urlError: String? = null,
    val isValid: Boolean = false
) {
}
