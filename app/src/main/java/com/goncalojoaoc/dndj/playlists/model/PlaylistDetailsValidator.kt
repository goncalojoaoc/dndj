package com.goncalojoaoc.dndj.playlists.model

import com.goncalojoaoc.dndj.R
import com.goncalojoaoc.dndj.strings.IStrings
import javax.inject.Inject

class PlaylistDetailsValidator @Inject constructor(
    private val strings: IStrings
){

    @Suppress("RegExpRedundantEscape")
    private val validUrlRegex = Regex("""(?:https?:\/\/)?(?:www\.)?youtu\.?be(?:\.com)?\/?.*(?:watch|embed)?(?:.*v=|v\/|\/)([\w\-_]+)\&?""")

    fun validate(model: PlaylistModel): PlaylistDetailsState {
        var urlError: String? = null
        var titleError: String? = null
        if (model.url.isBlank()) urlError = strings.getById(R.string.missing_video_url)
        if (model.name.isBlank()) titleError = strings.getById(R.string.missing_playlist_name)
        if (!model.url.matches(validUrlRegex)) urlError = strings.getById(R.string.invalid_video_url)
        return PlaylistDetailsState(
            titleError = titleError,
            urlError = urlError,
            isValid = titleError == null && urlError == null
        )
    }

}
