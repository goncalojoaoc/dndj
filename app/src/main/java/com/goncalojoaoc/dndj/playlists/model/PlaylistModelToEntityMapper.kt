package com.goncalojoaoc.dndj.playlists.model

import com.goncalojoaoc.dndj.playlists.entity.Playlist
import com.goncalojoaoc.dndj.playlists.entity.PlaylistSource
import javax.inject.Inject

class PlaylistModelToEntityMapper @Inject constructor() {

    fun map(model: PlaylistModel, folderId: Long): Playlist =
        Playlist(
            id = model.id,
            name = model.name,
            url = model.url,
            imageUri = model.imageUri,
            folderId = folderId,
            source = PlaylistSource.YOUTUBE
        )
}
