package com.goncalojoaoc.dndj.playlists.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.goncalojoaoc.dndj.ErrorTrackingViewModel
import com.goncalojoaoc.dndj.error.ErrorMapper
import com.goncalojoaoc.dndj.folders.model.FolderModel
import com.goncalojoaoc.dndj.folders.persistence.IFolderRepository
import com.goncalojoaoc.dndj.playlists.model.PlaylistModel
import com.goncalojoaoc.dndj.playlists.networking.TracksInteractor
import com.goncalojoaoc.dndj.playlists.repository.IPlaylistRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PlaylistsViewModel @Inject constructor(
    private val playlistRepository: IPlaylistRepository,
    foldersRepository: IFolderRepository,
    errorMapper: ErrorMapper,
    private val tracksInteractor: TracksInteractor
) : ErrorTrackingViewModel(errorMapper) {

    private var playlistsInFolder: LiveData<List<PlaylistModel>> = playlistRepository.getAll()
    private val _playlists = MediatorLiveData<List<PlaylistModel>>()
    val playlists: LiveData<List<PlaylistModel>> = _playlists

    val folders: LiveData<List<FolderModel>> = foldersRepository.getAll()

    fun loadFromFolder(folder: FolderModel) {
        _playlists.removeSource(playlistsInFolder)
        playlistsInFolder = playlistRepository.getAllInFolder(folder.id)
        _playlists.addSource(playlistsInFolder) { _playlists.value = it }
    }

    fun play(playlist: PlaylistModel) {
        viewModelScope.launch(Dispatchers.IO) {
            tracksInteractor.play(playlist).onFailure { onError(it) }
        }
    }

    fun delete(playlist: PlaylistModel) {
        viewModelScope.launch(Dispatchers.IO) {
            playlistRepository.delete(playlist)
        }
    }

}
