package com.goncalojoaoc.dndj.playlists.networking.response

data class TracksQueuedResponse(
    val songsQueued: Int
)
