package com.goncalojoaoc.dndj.playlists.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.goncalojoaoc.dndj.databinding.PlaylistViewBinding
import com.goncalojoaoc.dndj.playlists.model.PlaylistModel

class PlaylistAdapter(
    private val onPlaylistClicked: (PlaylistModel) -> Unit,
    private val onPlaylistDeleted: (PlaylistModel) -> Unit
) : ListAdapter<PlaylistModel, PlaylistViewHolder>(DiffCallback()) {

    private var editing: PlaylistModel? = null

    fun clearEditingState() {
        editing = null
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlaylistViewHolder {
        val binding = PlaylistViewBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return PlaylistViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PlaylistViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(
            playlist = item,
            onClick = onPlaylistClicked,
            onEditing = { edit(item) },
            isEditing = item == editing,
            onDelete = this::delete
        )
    }

    private fun delete(playlistModel: PlaylistModel) {
        clearEditingState()
        onPlaylistDeleted(playlistModel)
    }

    private fun edit(playlist: PlaylistModel) {
        if (editing == playlist) {
            clearEditingState()
        } else {
            editing = playlist
        }
        notifyDataSetChanged()
    }

}

private class DiffCallback : DiffUtil.ItemCallback<PlaylistModel>() {

    override fun areItemsTheSame(oldItem: PlaylistModel, newItem: PlaylistModel): Boolean =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: PlaylistModel, newItem: PlaylistModel): Boolean =
        oldItem == newItem

}
