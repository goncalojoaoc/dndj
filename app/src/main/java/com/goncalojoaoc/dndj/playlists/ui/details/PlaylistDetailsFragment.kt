package com.goncalojoaoc.dndj.playlists.ui.details

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.SimpleAdapter
import android.widget.SpinnerAdapter
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.goncalojoaoc.dndj.databinding.PlaylistDetailsFragmentBinding
import com.goncalojoaoc.dndj.folders.model.FolderModel
import com.goncalojoaoc.dndj.playlists.model.PlaylistDetailsState
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class PlaylistDetailsFragment : DialogFragment() {

    private val viewModel: PlaylistDetailsViewModel by viewModels()
    private val arguments: PlaylistDetailsFragmentArgs by navArgs()

    private lateinit var folderSpinnerAdapter: FolderSpinnerAdapter

    private lateinit var binding: PlaylistDetailsFragmentBinding

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = PlaylistDetailsFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
    }

    private fun setupViews() {
        folderSpinnerAdapter = FolderSpinnerAdapter(requireContext())
        with(binding) {
            urlInput.addTextChangedListener { viewModel.setUrl(it.toString()) }
            saveButton.setOnClickListener { save() }
            folderSpinner.adapter = folderSpinnerAdapter
        }
        viewModel.previewImageUrl.observe(viewLifecycleOwner, { loadPreviewImage(it) })
        viewModel.folders.observe(viewLifecycleOwner, {
            updateFolderSpinner(it)
        })
    }

    private fun updateFolderSpinner(folders: List<FolderModel>) {
        folderSpinnerAdapter.clear()
        folderSpinnerAdapter.addAll(folders)
        val index = folders.indexOfFirst { it.id == arguments.folderId }
        if (index >= 0) binding.folderSpinner.setSelection(index)
    }

    private fun save() {
        val name = binding.titleInput.text.toString()
        val url = binding.urlInput.text.toString()
        val folderId = binding.folderSpinner.selectedItemId
        viewLifecycleOwner.lifecycleScope.launch {
            val state = viewModel.create(name, url, folderId)
            if (state.isValid) {
                dismiss()
            } else {
                bindErrorViews(state)
            }
        }
    }

    private fun bindErrorViews(state: PlaylistDetailsState) = with(binding) {
        titleLayout.error = state.titleError
        urlLayout.error = state.urlError
    }

    private fun loadPreviewImage(url: String?) {
        url?.let {
            Glide.with(this).load(url).into(binding.imagePreview)
        }
    }

    private class FolderSpinnerAdapter(context: Context) :
        ArrayAdapter<FolderModel>(context, android.R.layout.simple_spinner_dropdown_item) {

        override fun getItemId(position: Int): Long {
            return getItem(position)?.id ?: -1
        }
    }
}
