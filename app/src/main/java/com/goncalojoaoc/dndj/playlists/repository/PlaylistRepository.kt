package com.goncalojoaoc.dndj.playlists.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import com.goncalojoaoc.dndj.persistence.room.dao.PlaylistDao
import com.goncalojoaoc.dndj.playlists.model.PlaylistModel
import com.goncalojoaoc.dndj.playlists.model.PlaylistModelMapper
import com.goncalojoaoc.dndj.playlists.model.PlaylistModelToEntityMapper
import javax.inject.Inject

class PlaylistRepository @Inject constructor(
    private val playlistDao: PlaylistDao,
    private val modelMapper: PlaylistModelMapper,
    private val modelToEntityMapper: PlaylistModelToEntityMapper
) : IPlaylistRepository {

    override fun getAll(): LiveData<List<PlaylistModel>> =
        playlistDao.getAll().map { modelMapper.map(it) }

    override fun getAllInFolder(folderId: Long): LiveData<List<PlaylistModel>> =
        playlistDao.getAllByFolderId(folderId).map { modelMapper.map(it) }

    override fun getById(): LiveData<PlaylistModel> {
        TODO("Not yet implemented")
    }

    override suspend fun create(model: PlaylistModel, folderId: Long): Long =
        playlistDao.insert(modelToEntityMapper.map(model, folderId))

    override suspend fun delete(model: PlaylistModel) {
        playlistDao.delete(model.id)
    }
}
