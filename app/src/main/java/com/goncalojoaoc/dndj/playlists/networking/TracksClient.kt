package com.goncalojoaoc.dndj.playlists.networking

import com.goncalojoaoc.dndj.playlists.networking.request.QueueTrackRequest
import com.goncalojoaoc.dndj.playlists.networking.response.TracksQueuedResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface TracksClient {

    @POST("api/v1/tracks/queue")
    suspend fun queue(@Body body: QueueTrackRequest): Response<TracksQueuedResponse>
}
