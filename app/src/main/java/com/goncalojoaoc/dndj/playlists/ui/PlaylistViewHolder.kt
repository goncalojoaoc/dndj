package com.goncalojoaoc.dndj.playlists.ui

import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.goncalojoaoc.dndj.R
import com.goncalojoaoc.dndj.databinding.PlaylistViewBinding
import com.goncalojoaoc.dndj.playlists.model.PlaylistModel
import com.goncalojoaoc.dndj.utils.show

class PlaylistViewHolder(
    private val binding: PlaylistViewBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(
        playlist: PlaylistModel,
        onClick: (PlaylistModel) -> Unit,
        onEditing: () -> Unit,
        isEditing: Boolean,
        onDelete: (PlaylistModel) -> Unit
    ) {
        with(binding) {
            name.text = playlist.name
            Glide.with(image).load(playlist.imageUri).into(image)
            bindEditMode(isEditing)
            playlistWrapper.setOnClickListener { onClick(playlist) }
            playlistWrapper.setOnLongClickListener {
                onEditing()
                true
            }
            delete.setOnClickListener { onDelete(playlist) }
        }
    }

    private fun bindEditMode(isEditing: Boolean) {
        with(binding) {
            delete.show(isEditing)
            playlistWrapper.elevation = playlistWrapper.context.resources.getDimension(
                if (isEditing) {
                    R.dimen.playlist_selected_elevation
                } else {
                    R.dimen.playlist_default_elevation
                }
            )
        }
    }
}
