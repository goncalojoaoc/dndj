package com.goncalojoaoc.dndj.playlists.model

import com.goncalojoaoc.dndj.playlists.entity.Playlist
import javax.inject.Inject

class PlaylistModelMapper @Inject constructor() {

    fun map(playlists: List<Playlist>): List<PlaylistModel> = playlists.map { map(it) }

    fun map(playlist: Playlist): PlaylistModel = PlaylistModel(
        id = playlist.id,
        url = playlist.url,
        name = playlist.name,
        imageUri = playlist.imageUri,
        source = PlaylistSourceModel.YOUTUBE
    )
}
