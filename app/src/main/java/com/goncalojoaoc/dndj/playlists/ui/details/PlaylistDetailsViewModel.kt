package com.goncalojoaoc.dndj.playlists.ui.details

import androidx.lifecycle.*
import com.goncalojoaoc.dndj.folders.model.FolderModel
import com.goncalojoaoc.dndj.folders.persistence.IFolderRepository
import com.goncalojoaoc.dndj.playlists.model.PlaylistDetailsState
import com.goncalojoaoc.dndj.playlists.model.PlaylistDetailsValidator
import com.goncalojoaoc.dndj.playlists.model.PlaylistModel
import com.goncalojoaoc.dndj.playlists.model.PlaylistSourceModel
import com.goncalojoaoc.dndj.playlists.repository.IPlaylistRepository
import com.goncalojoaoc.dndj.thumbnails.VideoThumbnailOrchestrator
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class PlaylistDetailsViewModel @Inject constructor(
    private val playlistRepository: IPlaylistRepository,
    foldersRepository: IFolderRepository,
    private val videoThumbnailOrchestrator: VideoThumbnailOrchestrator,
    private val validator: PlaylistDetailsValidator
) : ViewModel() {

    private val _url: MutableLiveData<String> = MutableLiveData()

    val previewImageUrl: LiveData<String?> = _url.switchMap { url ->
        liveData {
            emit(videoThumbnailOrchestrator.getVideoThumbnail(url))
        }
    }

    val folders: LiveData<List<FolderModel>> = foldersRepository.getAll()

    fun setUrl(url: String) {
        _url.postValue(url)
    }

    suspend fun create(name: String, url: String, folderId: Long): PlaylistDetailsState {
        val model = PlaylistModel(
            id = 0,
            name = name,
            url = url,
            imageUri = previewImageUrl.value.orEmpty(),
            source = PlaylistSourceModel.YOUTUBE
        )

        val modelState = validator.validate(model)

        if (modelState.isValid) {
            withContext(Dispatchers.IO) { playlistRepository.create(model, folderId) }
        }

        return modelState
    }

}
