package com.goncalojoaoc.dndj.playlists

import com.goncalojoaoc.dndj.playlists.repository.IPlaylistRepository
import com.goncalojoaoc.dndj.playlists.repository.PlaylistRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class PlaylistModule {

    @Binds
    abstract fun bindsRepository(impl: PlaylistRepository): IPlaylistRepository

}
