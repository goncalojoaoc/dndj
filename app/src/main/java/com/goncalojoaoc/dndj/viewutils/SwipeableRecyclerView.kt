package com.goncalojoaoc.dndj.viewutils

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.GestureDetector
import android.view.MotionEvent
import androidx.core.view.GestureDetectorCompat
import androidx.recyclerview.widget.RecyclerView
import kotlin.math.absoluteValue

class SwipeableRecyclerView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : RecyclerView(context, attrs, defStyleAttr) {

    private val gestureDetector = GestureDetectorCompat(context, GestureListener())

    var swipeListener: OnSwipeListener? = null

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(e: MotionEvent?): Boolean =
        if (gestureDetector.onTouchEvent(e)) {
            true
        } else {
            super.onTouchEvent(e)
        }

    private inner class GestureListener: GestureDetector.SimpleOnGestureListener() {
        override fun onFling(
            e1: MotionEvent?,
            e2: MotionEvent?,
            velocityX: Float,
            velocityY: Float
        ): Boolean {
            if (swipeListener == null || velocityX.absoluteValue < velocityY.absoluteValue) {
                // Vertical swipe, we don't care about those
                return false
            }
            if (velocityX > 0) {
                swipeListener?.onSwipeRight()
            } else {
                swipeListener?.onSwipeLeft()
            }
            return true
        }
    }

    interface OnSwipeListener {
        fun onSwipeLeft()
        fun onSwipeRight()
    }
}
