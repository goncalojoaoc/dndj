package com.goncalojoaoc.dndj.persistence.room.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.goncalojoaoc.dndj.playlists.entity.Folder
import com.goncalojoaoc.dndj.playlists.entity.Playlist

@Dao
abstract class PlaylistDao {

    @Query("SELECT count(*) FROM playlist")
    abstract suspend fun count(): Long

    @Query("SELECT * FROM playlist")
    abstract fun getAll(): LiveData<List<Playlist>>

    @Query("SELECT * FROM playlist WHERE folderId == :folderId")
    abstract fun getAllByFolderId(folderId: Long): LiveData<List<Playlist>>

    suspend fun isEmpty(): Boolean = count() == 0L

    @Insert
    abstract suspend fun insert(playlist: Playlist): Long

    @Query("DELETE FROM playlist WHERE id == :playlistId")
    abstract suspend fun delete(playlistId: Long)
}
