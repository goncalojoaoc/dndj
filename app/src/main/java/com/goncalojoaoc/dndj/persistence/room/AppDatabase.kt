package com.goncalojoaoc.dndj.persistence.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.goncalojoaoc.dndj.authentication.entity.Server
import com.goncalojoaoc.dndj.persistence.room.dao.FolderDao
import com.goncalojoaoc.dndj.persistence.room.dao.PlaylistDao
import com.goncalojoaoc.dndj.persistence.room.dao.ServerDao
import com.goncalojoaoc.dndj.playlists.entity.Folder
import com.goncalojoaoc.dndj.playlists.entity.Playlist

@Database(
    entities = [Playlist::class, Server::class, Folder::class],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {

    companion object {
        const val DATABASE_NAME = "app_database"
    }

    abstract fun playlistDao(): PlaylistDao

    abstract fun serverDao(): ServerDao

    abstract fun folderDao(): FolderDao
}