package com.goncalojoaoc.dndj.persistence.settings

import android.content.SharedPreferences
import androidx.core.content.edit
import androidx.lifecycle.LiveData
import com.goncalojoaoc.dndj.utils.SharedPreferenceStringLiveData
import javax.inject.Inject

class AppSettings @Inject constructor(
    private val sharedPreferences: SharedPreferences
) : IAppSettings {

    companion object {
        private const val KEY_SERVER_URL = "server_url"
        private const val KEY_API_KEY = "api_key"
        private const val KEY_NOW_PLAYING_REFRESH = "now_playing_refresh"

        private const val DEFAULT_NOW_PLAYING_REFRESH = 10000L // 10 seconds
    }

    override var serverUrl: String?
        get() = sharedPreferences.getString(KEY_SERVER_URL, null)
        set(value) {
            sharedPreferences.edit {
                putString(KEY_SERVER_URL, value)
            }
        }

    override var apiKey: String?
        get() = sharedPreferences.getString(KEY_API_KEY, null)
        set(value) {
            sharedPreferences.edit {
                putString(KEY_API_KEY, value)
            }
        }

    override val apiKeyLiveData: LiveData<String>
        get() = SharedPreferenceStringLiveData(sharedPreferences, KEY_API_KEY, "")

    override var nowPlayingRefreshTimer: Long
        get() = sharedPreferences.getLong(KEY_NOW_PLAYING_REFRESH, DEFAULT_NOW_PLAYING_REFRESH)
        set(value) {
            sharedPreferences.edit {
                putLong(KEY_NOW_PLAYING_REFRESH, value)
            }
        }
}
