package com.goncalojoaoc.dndj.persistence.settings

import androidx.lifecycle.LiveData

interface IAppSettings {

    var serverUrl: String?

    var apiKey: String?

    val apiKeyLiveData: LiveData<String>

    var nowPlayingRefreshTimer: Long

}
