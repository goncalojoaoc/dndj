package com.goncalojoaoc.dndj.persistence.room

import android.content.Context
import androidx.room.Room
import com.goncalojoaoc.dndj.persistence.room.dao.FolderDao
import com.goncalojoaoc.dndj.persistence.room.dao.PlaylistDao
import com.goncalojoaoc.dndj.persistence.room.dao.ServerDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RoomModule {

    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext appContext: Context): AppDatabase =
        Room.databaseBuilder(appContext, AppDatabase::class.java, AppDatabase.DATABASE_NAME).build()

    @Provides
    @Singleton
    fun providePlaylistDao(appDatabase: AppDatabase): PlaylistDao = appDatabase.playlistDao()

    @Provides
    @Singleton
    fun provideServerDao(appDatabase: AppDatabase): ServerDao = appDatabase.serverDao()

    @Provides
    @Singleton
    fun provideFolderDao(appDatabase: AppDatabase): FolderDao = appDatabase.folderDao()
}
