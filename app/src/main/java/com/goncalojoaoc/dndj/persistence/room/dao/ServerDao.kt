package com.goncalojoaoc.dndj.persistence.room.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.goncalojoaoc.dndj.authentication.entity.Server

@Dao
interface ServerDao {

    @Query("SELECT * FROM server")
    fun getAll(): LiveData<List<Server>>

    @Query("SELECT * FROM server WHERE apiKey == :apiKey")
    fun getByApiKey(apiKey: String): LiveData<Server?>

    @Insert
    suspend fun add(server: Server): Long

    @Delete
    suspend fun delete(server: Server)
}
