package com.goncalojoaoc.dndj.persistence.room

import com.goncalojoaoc.dndj.R
import com.goncalojoaoc.dndj.persistence.room.dao.FolderDao
import com.goncalojoaoc.dndj.persistence.settings.IAppSettings
import com.goncalojoaoc.dndj.playlists.entity.Folder
import com.goncalojoaoc.dndj.strings.IStrings
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

class SettingsBootstrap @Inject constructor(
    private val settings: IAppSettings,
    private val strings: IStrings,
    private val folderDao: FolderDao
) {

    companion object {
        private const val DEFAULT_SERVER = "https://dndj.asarius.duckdns.org/"
    }

    fun initIfNeeded() = GlobalScope.launch(Dispatchers.IO) {
        initAppSettings()
        initDefaultData()
    }

    private suspend fun initDefaultData() {
        if (folderDao.count() > 0) return
        folderDao.insertFolder(
            Folder(
                name = strings.getById(R.string.default_folder_name)
            )
        )
    }

    private fun initAppSettings() {
        if (settings.serverUrl == null) {
            settings.serverUrl = DEFAULT_SERVER
        }
    }

}