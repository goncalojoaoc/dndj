package com.goncalojoaoc.dndj.persistence.room.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.goncalojoaoc.dndj.playlists.entity.Folder

@Dao
interface FolderDao {

    @Query("SELECT count(*) FROM folder")
    suspend fun count(): Long

    @Insert
    suspend fun insertFolder(folder: Folder): Long

    @Update
    suspend fun update(folder: Folder)

    @Query("SELECT * FROM folder")
    fun getAll(): LiveData<List<Folder>>
}
