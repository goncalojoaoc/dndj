package com.goncalojoaoc.dndj.thumbnails.youtube

import com.google.gson.annotations.SerializedName

data class YoutubeEmbedResponse(
    @SerializedName("thumbnail_url")
    val thumbnailUrl: String
)

