package com.goncalojoaoc.dndj.thumbnails.youtube

import com.goncalojoaoc.dndj.networking.safeApiCall
import javax.inject.Inject

class YoutubeThumbnailInteractor @Inject constructor(
    private val client: YoutubeClient
) {

    suspend fun getVideoEmbed(url: String) = safeApiCall {
        client.getEmbedInfo(url)
    }
}
