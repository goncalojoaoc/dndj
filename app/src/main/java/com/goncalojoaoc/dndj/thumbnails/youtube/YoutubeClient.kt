package com.goncalojoaoc.dndj.thumbnails.youtube

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface YoutubeClient {

    @GET("oembed?format=json")
    suspend fun getEmbedInfo(@Query("url") videoUrl: String): Response<YoutubeEmbedResponse>
}
