package com.goncalojoaoc.dndj.thumbnails

import com.goncalojoaoc.dndj.thumbnails.youtube.YoutubeThumbnailInteractor
import javax.inject.Inject

class VideoThumbnailOrchestrator @Inject constructor(
    private val youtubeThumbnailInteractor: YoutubeThumbnailInteractor
) {

    suspend fun getVideoThumbnail(url: String): String? {
        // Only YouTube is supported for now
        // Inject interactors into a map for the future when we support multiple sites
        return youtubeThumbnailInteractor.getVideoEmbed(url).getOrNull()?.thumbnailUrl
    }
}
