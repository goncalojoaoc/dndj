package com.goncalojoaoc.dndj.thumbnails

import com.goncalojoaoc.dndj.thumbnails.youtube.YoutubeClient
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
@InstallIn(SingletonComponent::class)
object ThumbnailsModule {

    @Provides
    fun provideYoutubeClient(): YoutubeClient =
        Retrofit.Builder()
            .baseUrl("https://www.youtube.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(YoutubeClient::class.java)
}
