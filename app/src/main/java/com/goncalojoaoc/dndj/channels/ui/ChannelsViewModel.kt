package com.goncalojoaoc.dndj.channels.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.goncalojoaoc.dndj.ErrorTrackingViewModel
import com.goncalojoaoc.dndj.channels.model.GuildModel
import com.goncalojoaoc.dndj.channels.model.VoiceChannelModel
import com.goncalojoaoc.dndj.channels.networking.VoiceChannelsInteractor
import com.goncalojoaoc.dndj.error.ErrorMapper
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ChannelsViewModel @Inject constructor(
    private val voiceChannelsInteractor: VoiceChannelsInteractor,
    errorMapper: ErrorMapper
) : ErrorTrackingViewModel(errorMapper) {

    private val _voiceChannels: MutableLiveData<List<VoiceChannelModel>> = MutableLiveData()
    val voiceChannels: LiveData<List<VoiceChannelModel>> = _voiceChannels

    private val _currentVoiceChannel: MutableLiveData<VoiceChannelModel?> = MutableLiveData(null)
    val currentVoiceChannel: LiveData<VoiceChannelModel?> = _currentVoiceChannel

    private val _currentGuild: MutableLiveData<GuildModel> = MutableLiveData()
    val currentGuild: LiveData<GuildModel> = _currentGuild

    init {
        loadActiveChannel()
        loadVoiceChannels()
    }

    fun loadVoiceChannels() = viewModelScope.launch(Dispatchers.IO) {
        voiceChannelsInteractor.getVoiceChannels().doOrLaunchError {
            _voiceChannels.postValue(it)
        }
    }

    fun loadActiveChannel() {
        viewModelScope.launch(Dispatchers.IO) {
            voiceChannelsInteractor.currentVoiceChannel().doOrLaunchError {
                _currentVoiceChannel.postValue(it.currentVoiceChannel)
                _currentGuild.postValue(it.currentGuild)
            }
        }
    }

    fun isInVoiceChannel() = _currentVoiceChannel.value != null

    fun joinVoiceChannel(channelId: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val result = voiceChannelsInteractor.joinVoiceChannel(channelId)
            result.doOrLaunchError { joined ->
                _currentVoiceChannel.postValue(joined)
            }
        }
    }

    fun leaveVoiceChannel(channelId: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val result = voiceChannelsInteractor.leaveVoiceChannel(channelId)
            result.doOrLaunchError {
                _currentVoiceChannel.postValue(null)
            }
        }
    }
}
