package com.goncalojoaoc.dndj.channels.model

data class CurrentVoiceChannelModel(
    val currentVoiceChannel: VoiceChannelModel?,
    val currentGuild: GuildModel?
)
