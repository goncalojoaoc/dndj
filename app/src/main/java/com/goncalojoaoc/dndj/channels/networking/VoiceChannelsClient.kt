package com.goncalojoaoc.dndj.channels.networking

import com.goncalojoaoc.dndj.channels.model.CurrentVoiceChannelModel
import com.goncalojoaoc.dndj.channels.model.VoiceChannelModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface VoiceChannelsClient {

    @GET("api/v1/voiceChannels")
    suspend fun getVoiceChannels(): Response<List<VoiceChannelModel>>

    @GET("api/v1/voiceChannels/current")
    suspend fun currentVoiceChannel(): Response<CurrentVoiceChannelModel>

    @POST("api/v1/voiceChannels/join/{channelId}")
    suspend fun joinVoiceChannel(@Query("channelId") channelId: String): Response<VoiceChannelModel>

    @POST("api/v1/voiceChannels/leave/{channelId}")
    suspend fun leaveVoiceChannel(@Query("channelId") channelId: String): Response<VoiceChannelModel>
}
