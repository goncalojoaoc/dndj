package com.goncalojoaoc.dndj.channels.model

data class VoiceChannelModel(
    val id: String,
    val name: String
)
