package com.goncalojoaoc.dndj.channels.model

data class GuildModel(
    val id: String,
    val name: String
)
