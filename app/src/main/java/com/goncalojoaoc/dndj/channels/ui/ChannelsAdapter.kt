package com.goncalojoaoc.dndj.channels.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.goncalojoaoc.dndj.channels.model.VoiceChannelModel
import com.goncalojoaoc.dndj.databinding.VoiceChannelViewBinding

class ChannelsAdapter(
    private val joinChannelById: (String) -> Unit
) : ListAdapter<VoiceChannelModel, VoiceChannelViewHolder>(DiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VoiceChannelViewHolder {
        val binding = VoiceChannelViewBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return VoiceChannelViewHolder(binding)
    }

    override fun onBindViewHolder(holder: VoiceChannelViewHolder, position: Int) {
        holder.bind(getItem(position), joinChannelById)
    }

}

private class DiffCallback : DiffUtil.ItemCallback<VoiceChannelModel>() {

    override fun areItemsTheSame(oldItem: VoiceChannelModel, newItem: VoiceChannelModel): Boolean =
        oldItem.id == newItem.id

    override fun areContentsTheSame(
        oldItem: VoiceChannelModel,
        newItem: VoiceChannelModel
    ): Boolean =
        oldItem == newItem

}
