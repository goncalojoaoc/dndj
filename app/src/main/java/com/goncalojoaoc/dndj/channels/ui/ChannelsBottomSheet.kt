package com.goncalojoaoc.dndj.channels.ui

import androidx.lifecycle.Lifecycle.Event.ON_RESUME
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.goncalojoaoc.dndj.R
import com.goncalojoaoc.dndj.channels.model.GuildModel
import com.goncalojoaoc.dndj.channels.model.VoiceChannelModel
import com.goncalojoaoc.dndj.databinding.ChannelsBottomSheetBinding
import com.goncalojoaoc.dndj.strings.IStrings
import com.goncalojoaoc.dndj.utils.*
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.STATE_COLLAPSED
import com.google.android.material.bottomsheet.BottomSheetBehavior.STATE_EXPANDED

class ChannelsBottomSheet(
    private val binding: ChannelsBottomSheetBinding,
    lifecycleOwner: LifecycleOwner,
    private val viewModel: ChannelsViewModel,
    private val strings: IStrings
): LifecycleObserver {

    private val behavior = BottomSheetBehavior.from(binding.root)

    private val adapter = ChannelsAdapter(
        joinChannelById = ::joinChannel
    )

    init {
        viewModel.currentVoiceChannel.observe(lifecycleOwner, { updateCurrentChannel(it) })
        viewModel.currentGuild.observe(lifecycleOwner, { updateCurrentGuild(it) })
        viewModel.voiceChannels.observe(lifecycleOwner, { updateVoiceChannelList(it) })
        with(binding) {
            status.setOnClickListener {
                when (behavior.state) {
                    STATE_EXPANDED -> behavior.collapse()
                    STATE_COLLAPSED -> behavior.expand()
                    else -> { /* ignore */ }
                }
            }
            channelsRecyclerView.layoutManager =
                LinearLayoutManager(root.context, RecyclerView.VERTICAL, false)
            channelsRecyclerView.adapter = adapter
        }
        lifecycleOwner.lifecycle.addObserver(this)
    }

    @OnLifecycleEvent(ON_RESUME)
    fun onResume() {
        viewModel.loadActiveChannel()
        if (adapter.isEmpty) {
            viewModel.loadVoiceChannels()
        }
    }

    private fun updateVoiceChannelList(voiceChannels: List<VoiceChannelModel>?) {
        voiceChannels?.let {
            adapter.submitList(voiceChannels)
            if (voiceChannels.isEmpty()) updateEmptyState()
        }
        updateEmptyState()
    }

    private fun updateEmptyState() {
        with(binding) {
            channels.show(adapter.isNotEmpty)
            noVoiceChannels.show(adapter.isEmpty)
        }
    }

    private fun updateCurrentChannel(voiceChannelModel: VoiceChannelModel?) {
        with(binding) {
            if (voiceChannelModel != null) {
                status.text = strings.getById(R.string.connected_to, voiceChannelModel.name)
                disconnect.setOnClickListener { viewModel.leaveVoiceChannel(voiceChannelModel.id) }
                disconnect.show()
            } else {
                status.text = strings.getById(R.string.not_connected)
                disconnect.hide()
            }
        }
    }

    private fun updateCurrentGuild(guildModel: GuildModel) {
        binding.title.text = guildModel.name
    }

    private fun joinChannel(channelId: String) {
        viewModel.joinVoiceChannel(channelId)
        behavior.collapse()
    }
}
