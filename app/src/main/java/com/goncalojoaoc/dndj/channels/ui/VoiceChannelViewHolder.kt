package com.goncalojoaoc.dndj.channels.ui

import androidx.recyclerview.widget.RecyclerView
import com.goncalojoaoc.dndj.channels.model.VoiceChannelModel
import com.goncalojoaoc.dndj.databinding.VoiceChannelViewBinding

class VoiceChannelViewHolder(
    private val binding: VoiceChannelViewBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(model: VoiceChannelModel, join: (String) -> Unit) {
        binding.name.text = model.name
        binding.root.setOnClickListener { join(model.id) }
    }
}
