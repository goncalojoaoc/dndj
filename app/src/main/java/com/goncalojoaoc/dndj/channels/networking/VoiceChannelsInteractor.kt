package com.goncalojoaoc.dndj.channels.networking

import com.goncalojoaoc.dndj.channels.model.CurrentVoiceChannelModel
import com.goncalojoaoc.dndj.channels.model.VoiceChannelModel
import com.goncalojoaoc.dndj.networking.BotBackendClientManager
import com.goncalojoaoc.dndj.networking.safeApiCall
import javax.inject.Inject

class VoiceChannelsInteractor @Inject constructor(
    private val clientManager: BotBackendClientManager
) {

    suspend fun currentVoiceChannel(): Result<CurrentVoiceChannelModel> =
        safeApiCall { clientManager.voiceChannelsClient.currentVoiceChannel() }

    suspend fun getVoiceChannels(): Result<List<VoiceChannelModel>> =
        safeApiCall { clientManager.voiceChannelsClient.getVoiceChannels() }

    suspend fun joinVoiceChannel(channelId: String): Result<VoiceChannelModel> =
        safeApiCall { clientManager.voiceChannelsClient.joinVoiceChannel(channelId) }

    suspend fun leaveVoiceChannel(channelId: String): Result<VoiceChannelModel> =
        safeApiCall { clientManager.voiceChannelsClient.leaveVoiceChannel(channelId) }
}
