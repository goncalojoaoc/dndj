package com.goncalojoaoc.dndj.currently_playing.networking.response

data class TrackResponse(
    val title: String,
    val author: String,
    val length: Long,
    val identifier: String,
    val isStream: Boolean,
    val uri: String
)

