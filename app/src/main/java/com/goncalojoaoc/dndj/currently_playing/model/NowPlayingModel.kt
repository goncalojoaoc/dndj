package com.goncalojoaoc.dndj.currently_playing.model

data class NowPlayingModel(
    val currentTrackName: String,
    val isLooping: Boolean
)
