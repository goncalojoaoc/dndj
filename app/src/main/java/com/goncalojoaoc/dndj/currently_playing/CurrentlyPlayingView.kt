package com.goncalojoaoc.dndj.currently_playing

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.goncalojoaoc.dndj.R
import com.goncalojoaoc.dndj.databinding.CurrentlyPlayingViewBinding
import com.goncalojoaoc.dndj.utils.getActivity

class CurrentlyPlayingView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private var binding: CurrentlyPlayingViewBinding =
        CurrentlyPlayingViewBinding.inflate(LayoutInflater.from(context))

    private val viewModel: CurrentlyPlayingViewModel

    private lateinit var activity: AppCompatActivity

    init {
        createView()
        viewModel = initViewModel()
        bindViews()
    }

    fun setShouldPollForInfo(shouldPollForInfo: () -> Boolean) {
        viewModel.setShouldPollForInfo(shouldPollForInfo)
    }

    private fun bindViews() {
        with(binding) {
            nowPlaying.text = context.getString(R.string.now_playing, "")
            loop.setOnCheckedChangeListener { _, isChecked ->
                viewModel.setLooping(isChecked)
            }
        }
        viewModel.nowPlaying.observe(activity, { model ->
            with(binding) {
                nowPlaying.text = context.getString(R.string.now_playing, model.currentTrackName)
                loop.isChecked = model.isLooping
            }
        })
    }

    private fun createView() {
        layoutParams = LayoutParams(
            LayoutParams.MATCH_PARENT,
            LayoutParams.WRAP_CONTENT
        )
        addView(binding.root)
    }

    private fun initViewModel(): CurrentlyPlayingViewModel {
        val activity = context.getActivity()
        if (activity == null) {
            Log.e("CurrentlyPlayingView", "Unable to find Activity!")
            throw IllegalStateException("Unable to find Activity!")
        }
        this.activity = activity
        return ViewModelProvider(activity).get(CurrentlyPlayingViewModel::class.java)
    }
}
