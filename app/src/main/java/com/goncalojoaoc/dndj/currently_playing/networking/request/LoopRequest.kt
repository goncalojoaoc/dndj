package com.goncalojoaoc.dndj.currently_playing.networking.request

data class LoopRequest(
    val loop: Boolean
)
