package com.goncalojoaoc.dndj.currently_playing.networking

import com.goncalojoaoc.dndj.currently_playing.networking.request.LoopRequest
import com.goncalojoaoc.dndj.currently_playing.networking.response.NowPlayingResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface NowPlayingClient {

    @GET("api/v1/tracks/queue")
    suspend fun nowPlaying(): Response<NowPlayingResponse>

    @POST("api/v1/tracks/loop")
    suspend fun setLoop(@Body request: LoopRequest): Response<LoopRequest>
}
