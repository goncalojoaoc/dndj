package com.goncalojoaoc.dndj.currently_playing.networking.response

data class NowPlayingResponse(
    val currentlyPlaying: TrackResponse?,
    val queuedTracks: List<TrackResponse>,
    val isLooping: Boolean
)
