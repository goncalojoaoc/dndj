package com.goncalojoaoc.dndj.currently_playing

import android.util.Log
import androidx.lifecycle.*
import com.goncalojoaoc.dndj.currently_playing.model.NowPlayingModel
import com.goncalojoaoc.dndj.currently_playing.model.NowPlayingModelMapper
import com.goncalojoaoc.dndj.currently_playing.networking.NowPlayingInteractor
import com.goncalojoaoc.dndj.persistence.settings.IAppSettings
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.coroutines.coroutineContext

@HiltViewModel
class CurrentlyPlayingViewModel @Inject constructor(
    private val interactor: NowPlayingInteractor,
    appSettings: IAppSettings,
    private val modelMapper: NowPlayingModelMapper
) : ViewModel() {

    private val refreshDelay = appSettings.nowPlayingRefreshTimer

    private var shouldPollForInfo: (() -> Boolean)? = null

    val nowPlaying: LiveData<NowPlayingModel> = liveData(Dispatchers.IO) {
        while (coroutineContext.isActive) {
            refresh()
            delay(refreshDelay)
        }
    }

    fun setShouldPollForInfo(shouldPollForInfo: () -> Boolean) {
        this.shouldPollForInfo = shouldPollForInfo
    }

    fun setLooping(isLooping: Boolean) = viewModelScope.launch(Dispatchers.IO) {
        interactor.setLooping(isLooping)
    }

    private suspend fun LiveDataScope<NowPlayingModel>.refresh() {
        if (shouldPollForInfo?.invoke() != true) {
            emit(modelMapper.mapEmpty())
            return
        }
        interactor.getNowPlaying().onSuccess {
            emit(modelMapper.map(it))
        }
    }
}
