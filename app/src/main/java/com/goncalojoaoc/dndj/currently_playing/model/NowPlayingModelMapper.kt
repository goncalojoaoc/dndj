package com.goncalojoaoc.dndj.currently_playing.model

import com.goncalojoaoc.dndj.currently_playing.networking.response.NowPlayingResponse
import javax.inject.Inject

class NowPlayingModelMapper @Inject constructor() {

    fun mapEmpty() = NowPlayingModel(
        currentTrackName = "",
        isLooping = false
    )

    fun map(response: NowPlayingResponse): NowPlayingModel = NowPlayingModel(
        currentTrackName = response.currentlyPlaying?.title.orEmpty(),
        isLooping = response.isLooping
    )
}
