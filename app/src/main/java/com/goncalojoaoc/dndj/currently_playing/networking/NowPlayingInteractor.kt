package com.goncalojoaoc.dndj.currently_playing.networking

import com.goncalojoaoc.dndj.currently_playing.networking.request.LoopRequest
import com.goncalojoaoc.dndj.networking.BotBackendClientManager
import com.goncalojoaoc.dndj.networking.safeApiCall
import javax.inject.Inject

class NowPlayingInteractor @Inject constructor(
    private val clientManager: BotBackendClientManager
) {

    suspend fun getNowPlaying() = safeApiCall {
        clientManager.nowPlayingClient.nowPlaying()
    }

    suspend fun setLooping(loop: Boolean) = safeApiCall {
        clientManager.nowPlayingClient.setLoop(
            LoopRequest(loop)
        )
    }
}
