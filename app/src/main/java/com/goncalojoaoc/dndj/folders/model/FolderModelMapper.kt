package com.goncalojoaoc.dndj.folders.model

import com.goncalojoaoc.dndj.playlists.entity.Folder
import javax.inject.Inject

class FolderModelMapper @Inject constructor() {

    fun map(entity: Folder): FolderModel = FolderModel(
        id = entity.id,
        name = entity.name
    )

    fun mapEmpty(): FolderModel = FolderModel(
        id = 0,
        name = ""
    )
}
