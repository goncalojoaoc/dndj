package com.goncalojoaoc.dndj.folders.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.goncalojoaoc.dndj.R
import com.goncalojoaoc.dndj.databinding.FolderDetailsFragmentBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@AndroidEntryPoint
class FolderDetailsFragment : DialogFragment() {

    private lateinit var binding: FolderDetailsFragmentBinding
    private val viewModel: FolderDetailsViewModel by viewModels()
    private val args: FolderDetailsFragmentArgs by navArgs()

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FolderDetailsFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.init(args.folder)
        setupViews()
    }

    private fun setupViews() {
        with(binding) {
            nameInput.setText(viewModel.model.name)
            nameInput.addTextChangedListener {
                if (nameLayout.error != null && it != null && it.isNotBlank()) {
                    nameLayout.error = null
                }
            }
            saveButton.setOnClickListener {
                onSaveClicked()
            }
        }
    }

    private fun onSaveClicked() {
        with(binding) {
            val name = nameInput.text
            if (name == null || name.isBlank()) {
                nameLayout.error = getString(R.string.error_necessary_field)
            } else {
                save(name.toString())
            }
        }
    }

    private fun save(name: String) {
        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.IO) {
            viewModel.save(name)
            launch(Dispatchers.Main) {
                dismiss()
            }
        }
    }
}
