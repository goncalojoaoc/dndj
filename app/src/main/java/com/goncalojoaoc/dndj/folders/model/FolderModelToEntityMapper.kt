package com.goncalojoaoc.dndj.folders.model

import com.goncalojoaoc.dndj.playlists.entity.Folder
import javax.inject.Inject

class FolderModelToEntityMapper @Inject constructor() {

    fun map(model: FolderModel): Folder = Folder(
        id = model.id,
        name = model.name
    )
}
