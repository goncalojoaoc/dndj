package com.goncalojoaoc.dndj.folders.ui

import androidx.lifecycle.ViewModel
import com.goncalojoaoc.dndj.folders.model.FolderModel
import com.goncalojoaoc.dndj.folders.model.FolderModelMapper
import com.goncalojoaoc.dndj.folders.persistence.IFolderRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class FolderDetailsViewModel @Inject constructor(
    private val repository: IFolderRepository,
    private val modelMapper: FolderModelMapper
) : ViewModel() {

    private var editing: Boolean = false
    lateinit var model: FolderModel

    fun init(model: FolderModel?) {
        editing = model != null
        this.model = model ?: modelMapper.mapEmpty()
    }

    suspend fun save(name: String) {
        val newFolder = model.copy(name = name)
        if (editing) {
            repository.update(newFolder)
        } else {
            repository.create(newFolder)
        }
    }
}
