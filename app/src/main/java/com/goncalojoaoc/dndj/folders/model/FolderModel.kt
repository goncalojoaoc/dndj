package com.goncalojoaoc.dndj.folders.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FolderModel(
    val id: Long = 0,
    val name: String
) : Parcelable {
    override fun toString(): String = name
}
