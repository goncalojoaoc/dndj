package com.goncalojoaoc.dndj.folders.persistence

import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import com.goncalojoaoc.dndj.folders.model.FolderModel
import com.goncalojoaoc.dndj.folders.model.FolderModelMapper
import com.goncalojoaoc.dndj.folders.model.FolderModelToEntityMapper
import com.goncalojoaoc.dndj.persistence.room.dao.FolderDao
import com.goncalojoaoc.dndj.playlists.entity.Folder
import javax.inject.Inject

class FolderRepository @Inject constructor(
    private val dao: FolderDao,
    private val entityToModelMapper: FolderModelMapper,
    private val modelToEntityMapper: FolderModelToEntityMapper
): IFolderRepository {

    override fun getAll(): LiveData<List<FolderModel>> = dao.getAll().map { folders ->
        folders.map { folder -> entityToModelMapper.map(folder) }
    }

    override suspend fun create(model: FolderModel): Long = dao.insertFolder(
        modelToEntityMapper.map(model)
    )

    override suspend fun update(model: FolderModel) = dao.update(
        modelToEntityMapper.map(model)
    )
}
