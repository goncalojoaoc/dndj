package com.goncalojoaoc.dndj.folders

import com.goncalojoaoc.dndj.folders.persistence.FolderRepository
import com.goncalojoaoc.dndj.folders.persistence.IFolderRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class FoldersModule {

    @Binds
    abstract fun bindRepository(impl: FolderRepository): IFolderRepository
}
