package com.goncalojoaoc.dndj.folders.persistence

import androidx.lifecycle.LiveData
import com.goncalojoaoc.dndj.folders.model.FolderModel

interface IFolderRepository {

    fun getAll(): LiveData<List<FolderModel>>

    suspend fun create(model: FolderModel): Long

    suspend fun update(model: FolderModel)
}
