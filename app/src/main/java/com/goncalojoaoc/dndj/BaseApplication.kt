package com.goncalojoaoc.dndj

import android.app.Application
import com.goncalojoaoc.dndj.persistence.room.SettingsBootstrap
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject

@HiltAndroidApp
class BaseApplication : Application() {

    @Inject
    lateinit var bootstrap: SettingsBootstrap

    override fun onCreate() {
        super.onCreate()

        bootstrap.initIfNeeded()
    }
}
