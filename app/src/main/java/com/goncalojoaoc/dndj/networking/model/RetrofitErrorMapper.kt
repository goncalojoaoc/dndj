package com.goncalojoaoc.dndj.networking.model

import com.google.gson.Gson
import okhttp3.ResponseBody

object RetrofitErrorMapper {

    private val gson = Gson()

    fun mapErrorBody(body: ResponseBody?, httpCode: Int): Throwable {
        if (body == null) return NullPointerException("errorBody was null")
        return try {
            val errorModel = gson.fromJson(body.string(), ServerErrorModel::class.java)
            errorModel.asException(httpCode)
        } catch (ex: Exception) {
            ex
        }
    }

    private fun ServerErrorModel.asException(httpCode: Int?): Throwable = ApiException(
        httpCode = httpCode,
        message = message,
        serverMessage = serverMessage
    )

}