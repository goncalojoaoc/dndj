package com.goncalojoaoc.dndj.networking

import com.goncalojoaoc.dndj.persistence.settings.IAppSettings
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class AuthHeaderInterceptor @Inject constructor(
    private val appSettings: IAppSettings
) : Interceptor {

    companion object {
        private const val API_KEY_HEADER = "apiKey"
        private const val CONTENT_TYPE = "Content-Type"
        private const val APPLICATION_JSON = "application/json"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val request =
            chain.request().newBuilder()
                .header(API_KEY_HEADER, appSettings.apiKey.orEmpty())
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .build()
        return chain.proceed(request)
    }
}