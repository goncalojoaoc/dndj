package com.goncalojoaoc.dndj.networking.model

data class ServerErrorModel(
    val message: String,
    val serverMessage: String?
)
