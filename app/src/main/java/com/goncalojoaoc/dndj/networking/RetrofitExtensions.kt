package com.goncalojoaoc.dndj.networking

import com.goncalojoaoc.dndj.networking.model.RetrofitErrorMapper
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import retrofit2.Response

/**
 * Performs the function provided as the apiCall inside a coroutine. Any failure will be caught and
 * mapped by RetrofitErrorMapper if possible.
 *
 * @return a Kotlin Result wrapper
 */
suspend fun <T> safeApiCall(
    dispatcher: CoroutineDispatcher = Dispatchers.IO,
    apiCall: suspend () -> Response<T>
): Result<T> =
    withContext(dispatcher) {
        try {
            val response = apiCall()
            if (response.isSuccessful) {
                Result.success(response.body()!!)
            } else {
                Result.failure(RetrofitErrorMapper.mapErrorBody(
                    body = response.errorBody(),
                    httpCode = response.code()
                ))
            }
        } catch (ex: Exception) {
            when (ex) {
                is HttpException -> {
                    Result.failure(
                        RetrofitErrorMapper.mapErrorBody(ex.response()?.errorBody(), ex.code())
                    )
                }
                else -> Result.failure(ex)
            }
        }
    }
