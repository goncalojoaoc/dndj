package com.goncalojoaoc.dndj.networking.model

class ApiException(
    val httpCode: Int? = null,
    message: String?,
    val serverMessage: String? = null
) : Exception(message)
