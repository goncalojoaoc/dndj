package com.goncalojoaoc.dndj.networking

import com.goncalojoaoc.dndj.channels.networking.VoiceChannelsClient
import com.goncalojoaoc.dndj.currently_playing.networking.NowPlayingClient
import com.goncalojoaoc.dndj.persistence.settings.IAppSettings
import com.goncalojoaoc.dndj.playlists.networking.TracksClient
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BotBackendClientManager @Inject constructor(
    private val appSettings: IAppSettings,
    interceptor: AuthHeaderInterceptor
) {

    companion object {
        private const val DEFAULT_URL = "http://localhost"
    }

    private val okHttpClient: OkHttpClient = OkHttpClient.Builder()
        .addInterceptor(interceptor)
        .build()

    private lateinit var retrofit: Retrofit

    lateinit var voiceChannelsClient: VoiceChannelsClient
        private set

    lateinit var tracksClient: TracksClient
        private set

    lateinit var nowPlayingClient: NowPlayingClient
        private set

    init {
        init()
    }

    fun init() {
        retrofit = Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(appSettings.serverUrl ?: DEFAULT_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        voiceChannelsClient = retrofit.create(VoiceChannelsClient::class.java)
        tracksClient = retrofit.create(TracksClient::class.java)
        nowPlayingClient = retrofit.create(NowPlayingClient::class.java)
    }
}
