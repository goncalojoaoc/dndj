package com.goncalojoaoc.dndj.error.ui

import android.view.Gravity
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.transition.Slide
import androidx.transition.TransitionManager
import com.goncalojoaoc.dndj.databinding.ErrorBannerViewBinding
import com.goncalojoaoc.dndj.error.ErrorModel
import com.goncalojoaoc.dndj.error.WarningType
import com.goncalojoaoc.dndj.utils.hide
import com.goncalojoaoc.dndj.utils.show

class ErrorBanner(
    private val binding: ErrorBannerViewBinding
) : Observer<ErrorModel?> {

    val isShowing: Boolean
        get() = binding.root.visibility == VISIBLE

    override fun onChanged(error: ErrorModel?) {
        if (error == null) {
            hide()
        } else {
            onError(error)
        }
    }

    private fun onError(error: ErrorModel?) = error?.let {
        when (error.warningType) {
            WarningType.BANNER -> displayBanner(error.message)
            WarningType.TOAST -> displayToast(error.message)
        }
    }

    private fun displayToast(message: String) {
        Toast.makeText(
            binding.root.context,
            message,
            Toast.LENGTH_LONG
        ).show()
    }

    private fun displayBanner(message: String) {
        binding.errorMessage.text = message
        if (!isShowing) {
            (binding.root.parent as? ViewGroup)?.let { parent ->
                TransitionManager.beginDelayedTransition(parent, Slide(Gravity.TOP))
            }
            binding.root.show()
        }
    }

    private fun hide() {
        if (!isShowing) return

        (binding.root.parent as? ViewGroup)?.let { parent ->
            TransitionManager.beginDelayedTransition(parent, Slide(Gravity.TOP))
        }
        binding.root.hide()
    }
}