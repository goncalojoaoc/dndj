package com.goncalojoaoc.dndj.error.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.goncalojoaoc.dndj.ErrorTrackingViewModel
import com.goncalojoaoc.dndj.databinding.ErrorBannerViewBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
abstract class BaseErrorBannerFragment : Fragment() {

    protected abstract val errorBannerViewBinding: ErrorBannerViewBinding

    protected abstract val errorTrackingViewModel: ErrorTrackingViewModel

    private lateinit var errorBanner: ErrorBanner

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        errorBanner = ErrorBanner(errorBannerViewBinding)
        errorTrackingViewModel.error.observe(viewLifecycleOwner, errorBanner)
    }
}
