package com.goncalojoaoc.dndj.error

data class ErrorModel(
    val message: String,
    val warningType: WarningType
)


enum class WarningType {
    BANNER, TOAST
}