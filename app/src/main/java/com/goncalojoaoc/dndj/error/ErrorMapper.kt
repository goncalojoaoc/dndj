package com.goncalojoaoc.dndj.error

import android.util.Log
import com.goncalojoaoc.dndj.R
import com.goncalojoaoc.dndj.networking.model.ApiException
import com.goncalojoaoc.dndj.strings.IStrings
import java.net.ConnectException
import javax.inject.Inject

class ErrorMapper @Inject constructor(
    private val strings: IStrings
) {

    fun mapException(ex: Throwable?): ErrorModel? =
        when (ex) {
            null -> {
                Log.e("ErrorMapper", "null exception")
                null
            }
            is ApiException -> mapApiException(ex)
            is ConnectException -> mapServerConnectionError(ex)
            else -> mapGenericError(ex)
        }


    private fun mapGenericError(ex: Throwable): ErrorModel {
        Log.e("ErrorMapper", "${ex::class.simpleName} | Msg: ${ex.message}", ex)
        return ErrorModel(
            message = strings.getById(R.string.error_generic),
            warningType = WarningType.TOAST
        )
    }

    private fun mapServerConnectionError(ex: ConnectException): ErrorModel {
        Log.e("ErrorMapper", "ConnectException | Msg: ${ex.message}", ex)
        return ErrorModel(
            message = strings.getById(R.string.error_connection_to_server),
            warningType = WarningType.BANNER
        )
    }

    private fun mapApiException(ex: ApiException): ErrorModel {
        Log.e("ErrorMapper", "ApiException | Code: ${ex.httpCode} | Msg: ${ex.serverMessage}", ex)

        fun formatMessage(msg: String?) = msg ?: strings.getById(R.string.error_server)

        return when (ex.httpCode) {
            401 -> ErrorModel(
                message = strings.getById(R.string.error_no_api_key),
                warningType = WarningType.BANNER
            )
            403 -> ErrorModel(
                message = strings.getById(R.string.error_invalid_api_key),
                warningType = WarningType.BANNER
            )
            400 -> ErrorModel(message = formatMessage(ex.message), warningType = WarningType.TOAST)
            else -> ErrorModel(
                message = strings.getById(R.string.error_server),
                warningType = WarningType.TOAST
            )
        }
    }
}
