package com.goncalojoaoc.dndj

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.goncalojoaoc.dndj.channels.ui.ChannelsViewModel
import com.goncalojoaoc.dndj.databinding.MainActivityBinding
import com.goncalojoaoc.dndj.strings.IStrings
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: MainActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = MainActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}
