package com.goncalojoaoc.dndj

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.goncalojoaoc.dndj.error.ErrorMapper
import com.goncalojoaoc.dndj.error.ErrorModel

abstract class ErrorTrackingViewModel(
    private val errorMapper: ErrorMapper
) : ViewModel() {

    protected val errors: MutableLiveData<ErrorModel?> = MutableLiveData(null)

    val error: LiveData<ErrorModel?> = errors

    protected suspend fun <T> Result<T>.doOrLaunchError(action: suspend (T) -> Unit) {
        if (isSuccess) {
            errors.postValue(null)
            action(getOrNull()!!)
        } else {
            val ex = exceptionOrNull()
            onError(ex)
        }
    }

    protected fun onError(ex: Throwable?) {
        errors.postValue(errorMapper.mapException(ex))
    }
}
