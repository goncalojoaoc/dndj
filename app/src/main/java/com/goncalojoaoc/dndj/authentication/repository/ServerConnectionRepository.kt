package com.goncalojoaoc.dndj.authentication.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import androidx.lifecycle.switchMap
import com.goncalojoaoc.dndj.authentication.model.ServerModel
import com.goncalojoaoc.dndj.authentication.model.ServerModelMapper
import com.goncalojoaoc.dndj.authentication.model.ServerModelToEntityMapper
import com.goncalojoaoc.dndj.persistence.room.dao.ServerDao
import com.goncalojoaoc.dndj.persistence.settings.IAppSettings
import com.goncalojoaoc.dndj.utils.SharedPreferenceStringLiveData
import javax.inject.Inject

class ServerConnectionRepository @Inject constructor(
    private val dao: ServerDao,
    private val appSettings: IAppSettings,
    private val entityToModelMapper: ServerModelMapper,
    private val modelToEntityMapper: ServerModelToEntityMapper
) : IServerConnectionRepository {

    private val _currentApiKey: LiveData<String> by lazy { appSettings.apiKeyLiveData }

    override val allServers: LiveData<List<ServerModel>> = _currentApiKey.switchMap { activeApiKey ->
        dao.getAll().map { list -> list.map { entityToModelMapper.map(it, activeApiKey) } }
    }

    override fun setActive(model: ServerModel) {
        appSettings.apiKey = model.apiKey
        appSettings.serverUrl = model.serverUrl
    }

    override suspend fun create(model: ServerModel) {
        dao.add(modelToEntityMapper.map(model))
    }

    override suspend fun delete(model: ServerModel) {
        dao.delete(modelToEntityMapper.map(model))
    }
}