package com.goncalojoaoc.dndj.authentication.model

data class ServerModel(
    val name: String,
    val serverUrl: String,
    val apiKey: String,
    val isConnected: Boolean
)
