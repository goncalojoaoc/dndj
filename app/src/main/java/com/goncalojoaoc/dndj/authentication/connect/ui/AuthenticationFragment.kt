package com.goncalojoaoc.dndj.authentication.connect.ui

import android.Manifest
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts.RequestPermission
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.budiyev.android.codescanner.*
import com.goncalojoaoc.dndj.R
import com.goncalojoaoc.dndj.databinding.AuthenticationFragmentBinding
import com.goncalojoaoc.dndj.utils.showToast
import com.google.zxing.BarcodeFormat
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@AndroidEntryPoint
class AuthenticationFragment : Fragment() {

    private lateinit var binding: AuthenticationFragmentBinding
    private var codeScanner: CodeScanner? = null

    private val viewModel: AuthenticationViewModel by viewModels()

    private val requestPermissionLauncher =
        registerForActivityResult(RequestPermission()) { isGranted ->
            if (isGranted) {
                initCodeReader()
            }
        }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = AuthenticationFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requestCameraPermission()
        bindViews()
    }

    private fun bindViews() {
        with(binding) {
            serverUrlInput.setText(AuthenticationViewModel.DEFAULT_SERVER_URL)

            serverUrlInfo.setOnClickListener { showInfoDialog(R.string.info_server_url) }
            serverKeyInfo.setOnClickListener { showInfoDialog(R.string.info_server_key) }

            connectButton.setOnClickListener {
                viewModel.saveAndConnect(
                    serverNameInput.text.toString(),
                    serverUrlInput.text.toString(),
                    serverKeyInput.text.toString()
                )
                findNavController().popBackStack()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        codeScanner?.startPreview()
    }

    override fun onPause() {
        codeScanner?.releaseResources()
        super.onPause()
    }

    private fun requestCameraPermission() {
        when {
            ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.CAMERA
            ) == PERMISSION_GRANTED -> initCodeReader()
            shouldShowRequestPermissionRationale(Manifest.permission.CAMERA) -> showRationale()
            else -> requestPermissionLauncher.launch(Manifest.permission.CAMERA)
        }
    }

    private fun showRationale() {
        AlertDialog.Builder(requireContext())
            .setTitle(R.string.cam_permission_rationale_title)
            .setMessage(R.string.cam_permission_rationale_message)
            .setPositiveButton(R.string.ok) { _, _ ->
                requestPermissionLauncher.launch(Manifest.permission.CAMERA)
            }
            .setNegativeButton(R.string.no_thanks) { _, _ -> }
            .show()
    }

    private fun initCodeReader() {
        codeScanner = CodeScanner(requireContext(), binding.scanner)
        with(codeScanner!!) {
            camera = CodeScanner.CAMERA_BACK
            formats = listOf(BarcodeFormat.QR_CODE)
            autoFocusMode = AutoFocusMode.SAFE
            scanMode = ScanMode.SINGLE
            isAutoFocusEnabled = true
            isFlashEnabled = false
            decodeCallback = DecodeCallback {
                onCodeDecoded(it.text)
            }
            errorCallback = ErrorCallback {
                onCodeError(it)
            }
            startPreview()
        }
    }

    private fun onCodeDecoded(text: String?) =
        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            if (text == null) {
                // If no text was decoded, restart the preview and try again
                codeScanner?.startPreview()
                return@launch
            }
            binding.serverKeyInput.setText(text)
        }

    private fun onCodeError(exception: Exception) {
        Log.e("AuthenticationFragment", "Error decoding QR code", exception)
        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            showToast(R.string.error_reading_qr, Toast.LENGTH_LONG)
        }
    }

    private fun showInfoDialog(
        @StringRes msg: Int,
        cancellable: Boolean = true,
        onDismiss: (() -> Unit)? = null
    ) {
        AlertDialog.Builder(requireContext())
            .setMessage(msg)
            .setCancelable(cancellable)
            .setPositiveButton(R.string.ok) { _, _ -> onDismiss?.invoke() }
            .show()
    }
}
