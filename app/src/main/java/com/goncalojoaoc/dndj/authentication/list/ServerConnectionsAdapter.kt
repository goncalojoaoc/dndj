package com.goncalojoaoc.dndj.authentication.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.goncalojoaoc.dndj.authentication.model.ServerModel
import com.goncalojoaoc.dndj.databinding.ServerViewBinding

class ServerConnectionsAdapter(
    private val onServerActivated: (ServerModel) -> Unit
) : ListAdapter<ServerModel, ServerConnectionViewHolder>(DiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ServerConnectionViewHolder {
        val binding = ServerViewBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ServerConnectionViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ServerConnectionViewHolder, position: Int) {
        val model = getItem(position)
        holder.bind(model) {
            onServerActivated(model)
        }
    }

}

private class DiffCallback : DiffUtil.ItemCallback<ServerModel>() {

    override fun areItemsTheSame(oldItem: ServerModel, newItem: ServerModel): Boolean =
        oldItem.apiKey == newItem.apiKey

    override fun areContentsTheSame(oldItem: ServerModel, newItem: ServerModel): Boolean =
        oldItem == newItem

}