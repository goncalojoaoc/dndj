package com.goncalojoaoc.dndj.authentication

import com.goncalojoaoc.dndj.authentication.repository.IServerConnectionRepository
import com.goncalojoaoc.dndj.authentication.repository.ServerConnectionRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class AuthenticationModule {

    @Binds
    abstract fun bindRepository(impl: ServerConnectionRepository): IServerConnectionRepository
}
