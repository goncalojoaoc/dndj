package com.goncalojoaoc.dndj.authentication.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.goncalojoaoc.dndj.databinding.ConnectionsFragmentBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ServerConnectionsFragment : Fragment() {

    private val viewModel: ServerConnectionViewModel by viewModels()

    private lateinit var binding: ConnectionsFragmentBinding
    private lateinit var adapter: ServerConnectionsAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ConnectionsFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViews()
    }

    private fun bindViews() {
        adapter = ServerConnectionsAdapter(viewModel::onServerSelected)

        with(binding) {
            connections.layoutManager = LinearLayoutManager(
                requireContext(),
                RecyclerView.VERTICAL,
                false
            )
            connections.adapter = adapter
            addServerButton.setOnClickListener {
                findNavController().navigate(
                    ServerConnectionsFragmentDirections.actionServerConnectionsFragmentToAuthenticationFragment()
                )
            }
        }

        viewModel.serverConnections.observe(viewLifecycleOwner, { adapter.submitList(it) })
    }
}
