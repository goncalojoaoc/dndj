package com.goncalojoaoc.dndj.authentication.model

import com.goncalojoaoc.dndj.authentication.entity.Server
import javax.inject.Inject

class ServerModelMapper @Inject constructor() {

    fun map(entity: Server, activeApiKey: String) = ServerModel(
        name = entity.name,
        apiKey = entity.apiKey,
        serverUrl = entity.serverUrl,
        isConnected = entity.apiKey == activeApiKey
    )
}
