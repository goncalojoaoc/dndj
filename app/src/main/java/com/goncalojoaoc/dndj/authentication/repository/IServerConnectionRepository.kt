package com.goncalojoaoc.dndj.authentication.repository

import androidx.lifecycle.LiveData
import com.goncalojoaoc.dndj.authentication.model.ServerModel

interface IServerConnectionRepository {

    val allServers: LiveData<List<ServerModel>>

    fun setActive(model: ServerModel)

    suspend fun create(model: ServerModel)

    suspend fun delete(model: ServerModel)

}
