package com.goncalojoaoc.dndj.authentication.model

import com.goncalojoaoc.dndj.authentication.entity.Server
import javax.inject.Inject

class ServerModelToEntityMapper @Inject constructor() {

    fun map(model: ServerModel) = Server(
        name = model.name,
        apiKey = model.apiKey,
        serverUrl = model.serverUrl
    )
}
