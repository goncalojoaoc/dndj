package com.goncalojoaoc.dndj.authentication.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class Server(
    @PrimaryKey
    val apiKey: String,
    val name: String,
    val serverUrl: String
)
