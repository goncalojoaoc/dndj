package com.goncalojoaoc.dndj.authentication.connect.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.goncalojoaoc.dndj.authentication.model.ServerModel
import com.goncalojoaoc.dndj.authentication.repository.IServerConnectionRepository
import com.goncalojoaoc.dndj.networking.BotBackendClientManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AuthenticationViewModel @Inject constructor(
    private val repository: IServerConnectionRepository,
    private val clientManager: BotBackendClientManager
) : ViewModel() {

    companion object {
        const val DEFAULT_SERVER_URL = "https://dndj.asarius.duckdns.org/"
    }

    fun saveAndConnect(name: String, url: String, key: String) {
        val server = ServerModel(
            name = name,
            serverUrl = url,
            apiKey = key,
            isConnected = true
        )
        viewModelScope.launch(Dispatchers.IO) {
            repository.create(server)
            repository.setActive(server)
            clientManager.init()
        }

    }

}
