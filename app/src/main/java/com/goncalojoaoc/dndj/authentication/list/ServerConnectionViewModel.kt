package com.goncalojoaoc.dndj.authentication.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.goncalojoaoc.dndj.authentication.model.ServerModel
import com.goncalojoaoc.dndj.authentication.repository.ServerConnectionRepository
import com.goncalojoaoc.dndj.networking.BotBackendClientManager
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ServerConnectionViewModel @Inject constructor(
    private val serverConnectionRepository: ServerConnectionRepository,
    private val clientManager: BotBackendClientManager
) : ViewModel() {

    val serverConnections: LiveData<List<ServerModel>> = serverConnectionRepository.allServers

    fun onServerSelected(server: ServerModel) {
        serverConnectionRepository.setActive(server)
        clientManager.init()
    }
}