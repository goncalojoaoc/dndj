package com.goncalojoaoc.dndj.authentication.list

import androidx.recyclerview.widget.RecyclerView
import com.goncalojoaoc.dndj.authentication.model.ServerModel
import com.goncalojoaoc.dndj.databinding.ServerViewBinding
import com.goncalojoaoc.dndj.utils.show

class ServerConnectionViewHolder(
    private val binding: ServerViewBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(server: ServerModel, onServerActivated: () -> Unit) {
        binding.serverConnected.show(server.isConnected)
        binding.serverName.text = server.name
        binding.root.setOnClickListener {
            onServerActivated()
        }
    }
}
